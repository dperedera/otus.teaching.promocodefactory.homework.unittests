﻿using System;
using System.Linq;
using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly PartnersController _partnersController;
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotExists_ReturnsError404()
        {
            //arrange
            var partnerId = Guid.NewGuid();
            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partnerId)).ReturnsAsync(() => null);
            //act
            var act = await _partnersController
                .SetPartnerPromoCodeLimitAsync(partnerId, new SetPartnerPromoCodeLimitRequest());
            //assert
            act.Should().BeAssignableTo<NotFoundResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnsError400()
        {
            //arrange
            var partnerId = Guid.NewGuid();
            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partnerId))
                .ReturnsAsync(() => new Partner {IsActive = false});
            //act
            var act = await _partnersController
                .SetPartnerPromoCodeLimitAsync(partnerId, new SetPartnerPromoCodeLimitRequest());
            //assert
            act.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetLimitToPartnerWithActiveLimit_IssuedPromocodesCountChangedToZero()
        {
            var partner = PartnerBuilder.CreatePartner()
                .IsActive()
                .WithNumberIssuedPromoCodes(10)
                .WithActivePartnerLimit()
                .Build();
            var partnerId = partner.Id;
            
            var request = CreateSetPartnerPromoCodeLimitRequest();
            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partnerId)).ReturnsAsync(() => partner);
            //act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);
            //assert
            partner.NumberIssuedPromoCodes.Should().Be(0);
        }
        
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetLimitToPartnerWithoutActiveLimit_IssuedPromocodesCountIsUnchanged()
        {
            //arrange
            var partner = PartnerBuilder.CreatePartner()
                .IsActive()
                .WithNumberIssuedPromoCodes(10)
                .Build();
            var partnerId = partner.Id;

            var request = CreateSetPartnerPromoCodeLimitRequest();
            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partnerId)).ReturnsAsync(() => partner);
            //act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);
            //assert
            partner.NumberIssuedPromoCodes.Should().Be(10);
        }
        
        
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetLimitToPartnerWithActiveLimit_ExistingActiveLimitIsCancelled()
        {
            //arrange
            var partner = PartnerBuilder.CreatePartner()
                .IsActive()
                .WithNumberIssuedPromoCodes(10)
                .WithActivePartnerLimit()
                .Build();
            var partnerId = partner.Id;
            var limitId = partner.PartnerLimits.First().Id;

            var request = CreateSetPartnerPromoCodeLimitRequest();
            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partnerId)).ReturnsAsync(() => partner);
            //act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);
            //assert
            partner.PartnerLimits
                .First(x => x.Id == limitId)
                .CancelDate
                .Should()
                .HaveValue();
        }
        
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetZeroLimit_ReturnsError400()
        {
            //arrange
            var partner = PartnerBuilder.CreatePartner()
                .IsActive()
                .WithNumberIssuedPromoCodes(10)
                .WithActivePartnerLimit()
                .Build();
            var partnerId = partner.Id;
            
            var request = CreateSetPartnerPromoCodeLimitRequest(0);
            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partnerId)).ReturnsAsync(() => partner);
            //act
            var act = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);
            
            //assert
            act.Should().BeAssignableTo<BadRequestObjectResult>();
        }
        
        
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_CorrectLimit_LimitIsSavedToDb()
        {
            //arrange
            var partner = PartnerBuilder.CreatePartner()
                .IsActive()
                .WithNumberIssuedPromoCodes(10)
                .Build();
            var partnerId = partner.Id;
            
            var request = CreateSetPartnerPromoCodeLimitRequest();
            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partnerId)).ReturnsAsync(() => partner);
            //act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);
            //assert
            _partnersRepositoryMock.Verify(x => x.UpdateAsync(partner), Times.Once);
        }

        private static SetPartnerPromoCodeLimitRequest CreateSetPartnerPromoCodeLimitRequest(int limit = 1)
        {
            return new SetPartnerPromoCodeLimitRequest
            {
                Limit = limit,
                EndDate = DateTime.MaxValue
            };
        }
    }
}