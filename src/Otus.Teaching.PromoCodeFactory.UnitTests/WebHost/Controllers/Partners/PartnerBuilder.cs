﻿using System;
using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class PartnerBuilder
    {
        private readonly Partner _partner;

        private PartnerBuilder(Partner partner)
        {
            _partner = partner;
        }

        public static PartnerBuilder CreatePartner()
        {
            var partner = new Partner
            {
                Id = Guid.NewGuid(),
                NumberIssuedPromoCodes = 0,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
            };
            return new PartnerBuilder(partner);
        }

        public PartnerBuilder IsActive()
        {
            _partner.IsActive = true;
            return this;
        }

        public PartnerBuilder WithNumberIssuedPromoCodes(int count)
        {
            _partner.NumberIssuedPromoCodes = count;
            return this;
        }
        
        public PartnerBuilder WithActivePartnerLimit()
        {
            _partner.PartnerLimits.Add(new PartnerPromoCodeLimit
            {
                Id = Guid.NewGuid(),
                Limit = 1
            });
            return this;
        }

        public Partner Build()
        {
            return _partner;
        }
    }
}